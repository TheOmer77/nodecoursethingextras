import { Router } from 'https://deno.land/x/oak/mod.ts';

const router = new Router();

interface Todo {
  id: string;
  text: string;
}
let todos: Todo[] = [];

router.get('/todos', ({ response }) => {
  response.body = { todos };
});

router.post('/todos', async ({ request, response }) => {
  const data = await request.body().value;
  const newTodo: Todo = { id: new Date().toISOString(), text: data.text };
  todos.push(newTodo);
  response.body = {
    msg: 'Todo added successfully.',
    addedTodo: newTodo,
    todos,
  };
});

router.put('/todos/:todoId', async ({ request, response, params }) => {
  const data = await request.body().value;
  const todoId = params.todoId;
  const todoIndex = todos.findIndex((todo) => todo.id === todoId);
  if (todoIndex < 0) {
    response.status = 404;
    response.body = {
      errMsg: `Could not find todo for the ID '${todoId}'.`,
    };
    return;
  }
  todos[todoIndex] = { id: todoId, text: data.text } as Todo;
  response.body = { msg: 'Todo updated successfully.', todos };
});

router.delete('/todos/:todoId', async ({ response, params }) => {
  const todoId = params.todoId;
  todos = todos.filter((todo) => todo.id !== todoId);
  response.body = { msg: 'Todo deleted successfully.', todos };
});

export default router;
