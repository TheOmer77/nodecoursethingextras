import { Application } from 'https://deno.land/x/oak/mod.ts';
import todosRoutes from './routes/todos.ts';

const PORT = 3000;
const app = new Application();

app.use(async (ctx, next) => {
  console.log('I am middleware.');
  await next();
});

app.use(todosRoutes.routes());
app.use(todosRoutes.allowedMethods());

app.addEventListener('listen', () => {
  console.clear();
  console.log(`\x1b[42m\x1b[30mServer is listening on port ${PORT}.\x1b[0m`);
});

await app.listen({ port: PORT });
