// This is just copied from the typescript module lol

const express = require('express');
const router = express.Router();

let todos = [];

router.get('/todos', (req, res, next) => {
  res.json({ todos });
});

router.post('/todos', (req, res, next) => {
  const body = req.body;
  const newTodo = { id: new Date().toISOString(), text: body.text };
  todos.push(newTodo);
  return res
    .status(201)
    .json({ msg: 'Todo added successfully.', addedTodo: newTodo, todos });
});

router.put('/todos/:todoId', (req, res, next) => {
  const body = req.body;
  const params = req.params;
  const todoId = params.todoId;
  const todoIndex = todos.findIndex((todo) => todo.id === todoId);
  if (todoIndex < 0)
    return res
      .status(404)
      .json({ errMsg: `Could not find todo for the ID '${todoId}'.` });
  todos[todoIndex] = { id: todoId, text: body.text };
  return res.status(200).json({ msg: 'Todo updated successfully.', todos });
});

router.delete('/todos/:todoId', (req, res, next) => {
  const params = req.params;
  const todoId = params.todoId;
  todos = todos.filter((todo) => todo.id !== todoId);
  return res.status(200).json({ msg: 'Todo deleted successfully.', todos });
});

module.exports = router;
