const express = require('express');
const todoRoutes = require('./routes/todos');
const app = express();
const PORT = 3000;

app.use(express.json());

app.use(todoRoutes);

app.listen(PORT, () => {
  console.clear();
  console.log(`\x1b[42m\x1b[30mServer is listening on port ${PORT}.\x1b[0m`);
});
